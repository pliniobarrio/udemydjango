from django.http import HttpResponse
from django.shortcuts import render
import  operator
def homepage(request):
    return render(request, 'home.html', {'hithere': 'This is Me'})

def eggs(request):
    return HttpResponse('<h1>EGGS</h1>')

def count(request):
    fulltext = request.GET['fulltext']
    wordlist = fulltext.split()
    worddict = {}

    for word in wordlist:
        if word in worddict:
            #Increase
            worddict[word] += 1
        else:
            #add to dict
            worddict[word] = 1

    sortwords = sorted(worddict.items(), key=operator.itemgetter(1), reverse=True)
    return render(request, 'count.html', {'fulltext': fulltext, 'count': len(wordlist), 'worddict': sortwords})

def about(request):
    return render(request, 'about.html')